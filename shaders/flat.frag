#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec3 frag_color;
layout (location = 1) in vec3 frag_normal;
layout (location = 0) out vec4 uFragColor;

void main() 
{
	vec3 light_dir = vec3(100.0, 100.0, 0.0);
	float brightness = dot(frag_normal, normalize(light_dir));
	brightness = clamp(brightness, 0.0, 1.0);
	
   vec3 color = frag_color * brightness;
   uFragColor = vec4(color, 1.0);
}
