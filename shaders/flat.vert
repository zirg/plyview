#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 normal;

layout(std140, binding = 0) uniform buf {
	mat4 	MVP;
	mat4	M;
}   uniformBuf;

layout (location = 0) out vec3 frag_color;
layout (location = 1) out vec3 frag_normal;

void main() 
{
	vec4 vertex_Position = uniformBuf.MVP * position;
	mat3 normalMatrix = transpose(inverse(mat3(uniformBuf.M)));
	frag_normal = normalize(normalMatrix * vec3(normal));
	
	frag_color = vec3(0.22, 0.804, 1.0);
	gl_Position = vertex_Position;
	
   // GL->VK conventions
   //gl_Position.y = -gl_Position.y;
   //gl_Position.z = (gl_Position.z + gl_Position.w) / 2.0;
}
