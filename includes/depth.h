#pragma once

#ifndef PV_DEPTH_H
#define PV_DEPTH_H

#include <iostream>
#include <string>
#include <cstdint>

#include <vulkan/vulkan.h>
#include <vulkan/vk_sdk_platform.h>


#include "tools.h"
#include "context.h"
#include "physical_device.h"
#include "window.h"
#include "device.h"

namespace pv {
	/*!
	 * @class DepthBuf
	 * @brief represents a Z-buffer for use with a framebuffer
	 */
	class DepthBuf
	{
	public:
		/*!
		 * @func DepthBuf
		 * @brief Constructor
		 * @param[in] physical GPU to allocate the depth buffer to
		 * @param[in] device Vulkan device to use with
		 * @param[in] Buffer width in pixels
		 * @param[in] Buffer height in pixels
		 */
		DepthBuf(pv::PhysicalDevice& physical, pv::Device& device, uint32_t width, uint32_t height);
		~DepthBuf();
	private:
		void			createVk(); //!< Allocates depth buffer memory
		void			destroyVk(); //!< Fres depth buffer memory
	public:
		VkImage			vk_image; //!< Vulkan iamge type
		VkImageView		vk_view; //!< Vulkan view type from image
		VkDeviceMemory	vk_memory; //!< Allocated memory on GPU
		VkFormat		vk_format; //!< Depth buffer format
	private:
		uint32_t				_width; //!< Depth buffer width
		uint32_t				_height; //!< Depth buffer height
		pv::PhysicalDevice&		_phys; //<! ref to GPU
		pv::Device&				_device; //!< ref to vulkan device
	};
}

#endif