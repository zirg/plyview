#pragma once

#ifndef PV_VULKAN_TOOLS_H
#define PV_VULKAN_TOOLS_H

#ifdef _WIN32
#include <windows.h>
#include <shellapi.h>
#pragma comment(linker, "/subsystem:windows")
#endif

#include <cstdlib>
#include <vulkan/vulkan.h>

// Macro to get a procedure address based on a vulkan instance
#define GET_INSTANCE_PROC_ADDR(inst, entrypoint)										\
{																						\
    fp##entrypoint = (PFN_vk##entrypoint) vkGetInstanceProcAddr(inst, "vk"#entrypoint); \
    if (fp##entrypoint == NULL)															\
	{																					\
		std::cerr << "Can't find vk"#entrypoint << std::endl;							\
        exit(EXIT_FAILURE);																\
    }																					\
}

// Macro to get a procedure address based on a vulkan device
#define GET_DEVICE_PROC_ADDR(dev, entrypoint)								            \
{																			            \
    fp##entrypoint = (PFN_vk##entrypoint) vkGetDeviceProcAddr(dev, "vk"#entrypoint);    \
    if (fp##entrypoint == NULL)														    \
	{																					\
		std::cerr << "Can't find vk"#entrypoint << std::endl;							\
        exit(EXIT_FAILURE);																\
    }																					\
}

#define PV_ASSERT(condition, message)													\
{																						\
	if ((condition) == false) {															\
		std::cerr << message << std::endl;												\
		exit(EXIT_FAILURE);																\
	}																					\
}

#define PV_ASSERT_VK(code, expected, message)											\
{																						\
	if (code != expected) {																\
		std::cerr << message << std::endl;												\
		std::cerr << "Assert Vulkan has [" << vkResultToString(code)					\
				  << "]. Was expected [" << vkResultToString(expected) << "]"			\
				  << std::endl;															\
		exit(EXIT_FAILURE);																\
	}																					\
}



static char const*		vkResultToString(VkResult code) {
	static const char	*positive_code_strs[] = {
		"VK_SUCCESS",
		"VK_NOT_READY",
		"VK_TIMEOUT",
		"VK_EVENT_SET",
		"VK_EVENT_RESET",
		"VK_INCOMPLETE",
	};
	static const char	*negative_codes_strs[] = {
		"VK_NEGATIVE_CODE_UNKNOWN",
		"VK_ERROR_OUT_OF_HOST_MEMORY",
		"VK_ERROR_OUT_OF_DEVICE_MEMORY",
		"VK_ERROR_INITIALIZATION_FAILED",
		"VK_ERROR_DEVICE_LOST",
		"VK_ERROR_MEMORY_MAP_FAILED",
		"VK_ERROR_LAYER_NOT_PRESENT",
		"VK_ERROR_EXTENSION_NOT_PRESENT",
		"VK_ERROR_FEATURE_NOT_PRESENT",
		"VK_ERROR_INCOMPATIBLE_DRIVER",
		"VK_ERROR_TOO_MANY_OBJECTS",
		"VK_ERROR_FORMAT_NOT_SUPPORTED"
	};
	int32_t				int_code = (int32_t)code;
	
	if (int_code >= 0)
		return positive_code_strs[int_code];
	return negative_codes_strs[-int_code];
}

#endif