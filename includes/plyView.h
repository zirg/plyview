#pragma once

#ifndef PLY_VIEW_H_
#define PLY_VIEW_H_

#include "tools.h"
#include "context.h"
#include "physical_device.h"
#include "window.h"
#include "device.h"
#include "swapchain.h"
#include "mesh.h"
#include "depth.h"
#include "program.h"
#include "viewer.h"

#define PV_USAGE  "Usage: plyView -p <path_to_file.ply> [-v <path_to_vertex.spv> | -f <path_to_fragment.spv> | -s <xyz scaling float number>]"

#endif