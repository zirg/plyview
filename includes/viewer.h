#pragma once

#ifndef PV_VIEWER_H
#define PV_VIEWER_H

#include <iostream>
#include <cstdint>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <vulkan/vulkan.h>
#include <vulkan/vk_sdk_platform.h>
#include <linmath.h>
#include <ply.h>
#include <wingetopt.h>

#include "tools.h"
#include "context.h"
#include "physical_device.h"
#include "window.h"
#include "device.h"
#include "swapchain.h"
#include "mesh.h"
#include "depth.h"
#include "program.h"

typedef struct	s_uniformBuf
{
	float		mvp[4][4];
	float		m[4][4];
}				uniformbuf_t;

namespace  pv {

	class Viewer
	{
	public:
		/*!
		 * @fn Viewer
		 * @brief Default constructor
		 */
		Viewer();

		/*!
		 * @fn ~Viewer
		 * @brief Default destructor
		 */
		~Viewer();

		/*!
		 * @fn init
		 * @brief Initalize vulkan, window, command. MUST be called before running
		 * @param[in] plyFile The path to .ply file plyViewer has to display
		 * @return true if everything succeeded, false otherwise
		 */
		bool			init(char *plyFile);

		/*!
		 * @fn run
		 * @brief The plyViewer main loop
		 */
		void			run();

	private:
		/*!
		 * @fn pvCreateDrawCmd
		 * @brief Registers what's needed to render each frame in the swapchain
		 * @return true on success
		 */
		bool		pvCreateDrawCmd();

		/*!
		 * @fn pvBeginCommand
		 * @brief Begin to register command in the swapchain command buffer
		 * @param[in] frame the coresponding frame to register
		 * @return true if vkBeginCommand returns VK_SUCCESS, false otherwise
		 */
		bool		pvBeginCommand(uint32_t frame);

		/*!
		 * @fn pvEndComamnd
		 * @brief End register state of the swapchain framebuffer
		 * @param[in] frame Index of the targeted swapchain buffer
		 * @return Always true
		 */
		bool		pvEndCommand(uint32_t frame);
	public:
		/*!
		 * @fn pvResize
		 * @brief Call this function to resize the window
		 * @param[in] width New window width in pixels
		 * @param[in] height New window height in pixels
		 * @return true if resize is done
		 * @warning Not implemented
		 */
		bool		pvResize(uint32_t width, uint32_t height);

		/*!
		 * @fn pvClose
		 * @brief SHOULD be called on window closing
		 * @warning, nothing happens but it might change
		 * @return Always true
		 */
		bool		pvClose(uint32_t exit_status);

		/*!
		 * @fn pvDraw
		 * @brief Issues render and present operation via vulkan API
		 * @return true on success
		 */
		bool		pvDraw(void);

		/*!
		 * @fn pvRotate
		 * @brief Update the uniform buffer memory, which has been rotated, hence this name
		 * @param[in] uni Data to be updated
		 * @return true
		 */
		bool		pvRotate(uniformbuf_t& uni);

	private:

		pv::Context							*_ctx; //!< Vulkan instance
		pv::PhysicalDevice					*_phys; //!< Vulkan PhysicalDevice
		pv::WindowKHR						*_window; //!< WSI wrapper
		pv::Device							*_device;//!< Vulkan device
		pv::SwapChain						*_swapchain; //!< Swapchain for presentation
		pv::Mesh							*_mesh; //!< Mesh from .ply file
		pv::FlatRenderer					*_render; //!< Use a flat render for this app

		VkDescriptorSet						_desc_set; //!< Descriptor set used to bind uniform memeory
		VkBuffer							_uniform_buf; //!< Vulkan Uniform buffer
		VkDeviceMemory						_uniform_buf_mem; //!< Uniform memory on gpu, corresponding with buffer
		VkDescriptorBufferInfo				_uniform_desc_info; //!< Information about uniform buffer
	};

}


#endif
