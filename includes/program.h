#pragma once

#ifndef PV_PROGRAM_H
#define PV_PROGRAM_H

#include <iostream>
#include <string>
#include <cstdint>
#include <fstream>

#include <vulkan/vulkan.h>
#include <vulkan/vk_sdk_platform.h>

#include "tools.h"
#include "context.h"
#include "physical_device.h"
#include "window.h"
#include "device.h"

namespace pv {
	/*!
	 * @class Program
	 * @brief Abstract class for well known fixed 
	 * pipeline with optionnal stages
	 */
	class Program {
	public:
		Program(pv::Device& device);
		virtual ~Program();

		/*!
		 * @func createSpvShader
		 * @brief Reads and allocate a VkShaderModule from a SPIR-V shader file
		 * @param[in] filename Full path of the shader file
		 * @param[out] shader The shader module to be created
		 * @return true on success
		 */
		bool						createSpvShader(const std::string& filename, VkShaderModule *shader);

		/*!
		 * @func bind
		 * @brief Bind the command to use this program
		 * @param[in] cmd in use command buffer
		 */
		virtual void				bind(VkCommandBuffer& cmd);

		/*!
		 * @func createVkPipeline
		 * @brief interface for pipeline creation
		 * @param[in] renderpass this pipeline will be compatible with this renderpass type
		 * @param[in] subpass The subpass index in the renderpass this program will be compatible with
		 */
		virtual void				createVkPipeline(VkRenderPass& renderpass, uint32_t subpass) = 0;
	protected:
		/*!
		 * @func createVklayout
		 * @brief Interface for create program shader layout
		 */
		virtual void				createVkLayout() = 0;

		/*!
		 * @func createVkPipelineInfos
		 * @brief interface for initialize pipeline creation
		 */
		virtual void				createVkPipelineInfos();

		/*!
		 * @func createVkDescriptorPool
		 * @brief interface for allcoating the program descriptor pool
		 */
		virtual void				createVkDecriptorPool() = 0;

	public:
		VkPipeline				vk_pipeline; //!< Vulkan pipeline native type
		VkPipelineLayout		vk_pipeline_layout;
		VkPipelineCache			vk_pipeline_cache;
		VkDescriptorSetLayout	vk_desc_layout;
		VkDescriptorPool		vk_desc_pool;
	protected:
		VkPipelineVertexInputStateCreateInfo	_vi;
		VkPipelineInputAssemblyStateCreateInfo	_ia;
		VkPipelineRasterizationStateCreateInfo	_rs;
		VkPipelineColorBlendStateCreateInfo		_cb;
		VkPipelineDepthStencilStateCreateInfo	_ds;
		VkPipelineViewportStateCreateInfo		_vp;
		VkPipelineMultisampleStateCreateInfo	_ms;
		VkDynamicState							_dynamicStateEnables[VK_DYNAMIC_STATE_RANGE_SIZE];
		VkPipelineDynamicStateCreateInfo		_dynamicState;
		VkGraphicsPipelineCreateInfo			_create_infos;

		pv::Device&		_device;
	};

	/*!
	 * @class FlatProgram
	 * @brief Implements Program with flat shading
	 * uses flat.frag.spv & flat.vert.spv
	 */
	class FlatProgram : public Program {
	public:
		FlatProgram(pv::Device& device);
		~FlatProgram();

		/*!
		* @func createVkPipeline
		* @brief Creates a pipeline with the flat vertex and fragment shaders
		* @param[in] renderpass this pipeline will be compatible with this renderpass type
		* @param[in] subpass The subpass index in the renderpass this program will be compatible with
		*/
		virtual void				createVkPipeline(VkRenderPass& renderpass, uint32_t subpass);
	protected:
		virtual void				createVkPipelineInfos();
		/*!
		 * @func createVklayout
		 * @brief Create the program layout for the unique uniform attribute
		 */
		virtual void				createVkLayout();
		
		/*!
		 * @func createVkDescriptorPool
		 * @brief Create a descriptor pool to allocate the unique uniform buffer
		 */
		virtual void				createVkDecriptorPool();
	public:
	private:
		VkVertexInputAttributeDescription		_vi_attr[2]; //!< pipeline has 2 inputs
		VkVertexInputBindingDescription			_vi_bind[2]; //!< pipeline has one binding per input (total 2)
	};
	extern std::string		flat_fragment_shader;
	extern std::string		flat_vertex_shader;

}

#endif