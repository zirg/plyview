#pragma once

#ifndef PV_WINDOW_KHR_H
#define PV_WINDOW_KHR_H

#include <iostream>
#include <cstdint>
#include <vulkan/vulkan.h>
#include <vulkan/vk_sdk_platform.h>

#include "context.h"
#include "physical_device.h"
#include "tools.h"

#ifdef _WIN32
extern HINSTANCE			g_connection;
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
#else
extern xcb_connection_t		*g_connection;
extern xcb_screen_t			*g_screen;
#endif

namespace pv {
	/*!
	 * @class WindowKHR
	 * @brief WindowKHR represent a window on the screen
	 * it is mainly used for presentation. Is defined with multi-platform support
	 */
	class WindowKHR
	{
	public:
		/*!
		 * @func WindowKHR
		 * @brief Constructor with default width = 800 and height = 600
		 * @param[in] context Vulkan context is required to create surface
		 * @param[in] physical_device The GPU specifics are also required
		 */
		WindowKHR(pv::Context& context, pv::PhysicalDevice& physical_device);

		/*!
		* @func WindowKHR
		* @brief Explicit constructor
		* @param[in] context Vulkan context is required to create surface
		* @param[in] physical_device The GPU specifics are also required
		* @param[in] width Initial window width in pixels
		* @param[in] height Initial window height in pixels
		*/
		WindowKHR(pv::Context& context, pv::PhysicalDevice& physical_device, int width, int height);


		/*!
		 * @func ~WindowKHR
		 * @brief Default Destructor
		 */
		~WindowKHR();

		/*!
		 * @func initWindow
		 * @brief Creates the window
		 */
		bool		initWindow();

		/*!
		 * @func initSurface
		 * @brief Create the surface within the window
		 */
		bool		initSurface();
		/*!
		 * @func initFormatAndColor
		 * @brief Assign surface format and color space default values
		 */
		bool		initFormatAndColor();

	public:
		uint32_t				width; //!< The window width in pixels
		uint32_t				height;//!< The window height in pixels
		VkSurfaceKHR			vk_surface; //!< Vulkan surface native type
		VkFormat				vk_format; //!< Vulkan format native type
		VkColorSpaceKHR			vk_color_space; //!< Vulkan color space

		PFN_vkGetPhysicalDeviceSurfaceSupportKHR			fpGetPhysicalDeviceSurfaceSupportKHR;
		PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR		fpGetPhysicalDeviceSurfaceCapabilitiesKHR;
		PFN_vkGetPhysicalDeviceSurfaceFormatsKHR			fpGetPhysicalDeviceSurfaceFormatsKHR;
		PFN_vkGetPhysicalDeviceSurfacePresentModesKHR		fpGetPhysicalDeviceSurfacePresentModesKHR;
#ifdef _WIN32
		HWND					window; //!< Window handle on windows OS
#else
		xcb_window_t			window; //!< Window handle on Linux platform
		xcb_intern_atom_reply_t *atom_wm_delete_window;
#endif
	private:
		pv::Context&			_ctx; //!< Vulkan instance ref
		pv::PhysicalDevice&		_phys; //!< GPU specifics
	};
}

#endif