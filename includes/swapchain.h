#pragma once

#ifndef PV_SWAP_CHAIN_H
#define PV_SWAP_CHAIN_H

#include <iostream>
#include <cstdint>
#include <vulkan/vulkan.h>
#include <vulkan/vk_sdk_platform.h>

#include "context.h"
#include "physical_device.h"
#include "window.h"
#include "device.h"
#include "renderer.h"
#include "depth.h"

namespace pv {
	/*!
	 * @class SwapChain
	 * @brief Wrapper for vulkan swapchain
	 * has images, views, commands, depthbuffer and framebuffer
	 * used for rendering and presenting
	 */
	class SwapChain
	{
	public:
		/*!
		 * @func Swapchain
		 * @brief Swapchain constructor initialize every objects needed for rendering and presenting
		 * @param[in] physical GPU specifics required to use memory
		 * @param[in] window The window to present to
		 * @param[in] renderer A reference to a compatible rendering strategy. (VkRenderpass)
		 */
		SwapChain(pv::PhysicalDevice& physical, pv::WindowKHR& window, Device& device, pv::Renderer& renderer);
		~SwapChain();

		/*!
		 * @func getSwapChainImageCount
		 * @brief Returning the size of allocated vulkan native types arrays
		 * @return The number of frames in this swapchain
		 */
		uint32_t			getSwapChainImageCount();
		
	private:
		VkSwapchainKHR		createSwapChain();
		uint32_t			countImages();
		void				createImages();
		void				createViews();
		void				createCommands();
		void				createFramebuffers();

	public:
		VkSwapchainKHR		vk_swapchain; //!< Vulkan native swapchain
		VkImage				*vk_images; //!< Array of vulkan native images
		VkImageView			*vk_views; //!< Array of vulkan native images view correponding to images
		VkCommandBuffer		*vk_commands; //!< Array of vulkan command buffer, one per image
		VkFramebuffer		*vk_framebuffers;// !< Array of framebuffers, one per image
	private:
		uint32_t			_image_count; //!< Number of image in this swapchain

		pv::DepthBuf			*_depth; //!< Depth buffer image and view ref, used by all framebuffers

		pv::PhysicalDevice&		_phys;
		pv::WindowKHR&			_win;
		pv::Device&				_device;
		pv::Renderer&			_renderer;
	};

}
#endif
