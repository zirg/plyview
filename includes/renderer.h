#pragma once

#ifndef PV_RENDERER_H
#define PV_RENDERER_H

#include <iostream>
#include <string>
#include <cstdint>

#include <vulkan/vulkan.h>
#include <vulkan/vk_sdk_platform.h>

#include "tools.h"
#include "context.h"
#include "physical_device.h"
#include "window.h"
#include "device.h"
#include "program.h"

#include "mesh.h"

namespace pv {
	/*!
	 * @class Renderer
	 * @brief Abstract class for a rendering process
	 * using single or multiple subpasses
	 */
	class Renderer
	{
	public:
		/*!
		 * @func Renderer
		 * @brief Renderer constructor will store parameters references for later use
		 */
		Renderer(pv::Device& device, pv::WindowKHR& window);
		
		virtual ~Renderer();

		/*!
		 * @func renderToCdm
		 * @brief Public renderer interface to register commands
		 * @param[out] cmd The command to register to, MUST be in the USE state
		 * @param[out] frame A framebuffer to render to
		 * @param[in] mesh The geometry to render
		 * @param[in] set Descriptor set to pass to the shader
		 */
		virtual void		renderToCmd(VkCommandBuffer& cmd, VkFramebuffer& frame, pv::Mesh& mesh, VkDescriptorSet& set) = 0;

	protected:
		/*!
		 * @func createVkRenderPass 
		 * @brief VkRenderpass native type allocation is specific by implementation
		 */
		virtual void		createVkRenderPass() = 0;
		
		/*!
		 * @func beginVkRenderPass
		 * @brief begining a renderpass is specific by implementation
		 * @param[in,out] cmd The command to register to, MUST be in the USE state
		 * @param[in,out] frame The frame to render to
		 */
		virtual void		beginVkRenderPass(VkCommandBuffer& cmd, VkFramebuffer& frame) = 0;
		/*!
		 * @func endVkRenderPass
		 * @brief Ends the last renderpass / subpass
		 */
		virtual void		endVkRenderPass(VkCommandBuffer& cmd);
	public:
		VkViewport			vk_viewport; //!< Always using a viewport
		VkRect2D			vk_scissor; //!< Always using a scisor
		VkRenderPass		vk_renderpass; //!< Native vulkan rendarpass

		uint32_t				vk_subpasses_count; //!< Number of subpasses
		VkSubpassDescription*	vk_subpasses; //!< Array of subpasses descriptions

		uint32_t				pv_programs_count; //!< Number of programgs
		pv::Program				*pv_programs; //!< Array of programs
	protected:
		pv::Device&			_device;
		pv::WindowKHR&		_win;
	};

	/*!
	 * @class FlatRenderer
	 * @brief Implements renderer with one subpass using FlatProgram
	 */
	class FlatRenderer : public Renderer
	{
	public:
		FlatRenderer(pv::Device& device, pv::WindowKHR& window);
		~FlatRenderer();

		/*!
		 * @func renderToCmd
		 * @brief Render the mesh using the FlatProgram
		 * @param[out] cmd The command to register to, MUST be in the USE state
		 * @param[out] frame A framebuffer to render to
		 * @param[in] mesh The geometry to render
		 * @param[in] set Descriptor set to pass to the shader
		 */
		virtual void		renderToCmd(VkCommandBuffer& cmd, VkFramebuffer& frame, pv::Mesh& mesh, VkDescriptorSet& set);
	protected:
		/*!
		 * @func createVkRenderpass
		 * @brief Flat program with one subpass specific renderpass creation
		 */
		void		createVkRenderPass();
		
		/*!
		* @func beginVkRenderPass
		* @brief begining a renderpass is specific by implementation
		* @param[in,out] cmd The command to register to, MUST be in the USE state
		* @param[in,out] frame The frame to render to
		*/
		void		beginVkRenderPass(VkCommandBuffer& cmd, VkFramebuffer& frame);
	private:
		VkClearValue				_clear[2]; //!< Color & depth clear values
		VkAttachmentDescription		_attachments[2]; //!< Color & Depth description
		VkAttachmentReference		_color_reference; //!< Color attachement details
		VkAttachmentReference		_depth_reference;//!< Depth attachement details
	};

}

#endif