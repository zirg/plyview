#pragma once

#ifndef PV_MESH_H
#define PV_MESH_H

#include <iostream>
#include <string>
#include <cstdint>
#include <vector>
#include <map>

#include <vulkan/vulkan.h>
#include <vulkan/vk_sdk_platform.h>

#include <ply.h>
#include <linmath.h>

#include "tools.h"
#include "context.h"
#include "physical_device.h"
#include "window.h"
#include "device.h"

typedef struct		s_plyVertex
{
	float			x;
	float			y;
	float			z;
	float			w;
}					ply_vertex_t;

typedef struct		s_plyFace
{
	unsigned char	n_indices;
	int				*indices;
}					ply_face_t;


namespace pv {
	/*!
	 * @class Mesh
	 * @brief Container for the geometry data and VBOs, SBOs
	 */
	class Mesh
	{
	public:

		Mesh(pv::PhysicalDevice& phys, pv::Device& device);
		~Mesh();

		/*!
		 * @func loadPlyFile
		 * @brief Fills the geometry buffers from a .ply file
		 * @param[in] filename full path of the file
		 * @return true on success
		 */
		bool					loadPlyFile(const std::string& filename);
	
		/*!
		 * @func vk_buffer_count
		 * @brief Getter on the number a vertex attributes
		 * @return The number 
		 */
		uint32_t				vk_buffer_count() const;
		
		/*!
		 * @func vk_indices_count
		 * @brief Count the number a indices
		 * @return Number of indices
		 */
		uint32_t				vk_indices_count() const;
		
		/*!
		 * @func vk_buffers
		 * @brief getter on the attributes buffers
		 * @return Array of attributes buffers
		 */
		VkBuffer*				vk_buffers();
		/*!
		 * @func vk_offets
		 * @brief Getter on the attributes buffers offsets to use the data from
		 * @return Array of vk_buffer_count offsets
		 */
		VkDeviceSize*			vk_offsets();

		/*!
		 * @func vk_indices
		 * @brief Getter on the index buffer
		 * @return the Index buffer
		 */
		VkBuffer&				vk_indices();
	private:
		/*!
		 * @func loadVertices
		 * @brief Reads, store & copy to vulkan buffer the vertices from opened .ply
		 * @param[in] file Opened .ply file
		 * @return true if at least one as been found
		 */
		bool					loadVertices(PlyFile * file);

		/*!
		 * @func generateNormals
		 * @brief Clears and computes normals from vertices and indices
		 * @warning Is quite long : O(n_vertices * n_indices)
		 */
		void					generateNormals();

		/*!
		* @func loadNormals
		* @brief Reads, store & copy to vulkan buffer the normals from opened .ply
		* @param[in] file Opened .ply file
		* @return true if at least one as been found
		*/
		bool					loadNormals(PlyFile * file);

		/*!
		* @func loadIndices
		* @brief Reads, store & copy to vulkan buffer the indices from opened .ply
		* @param[in] file Opened .ply file
		* @return true if at least one as been found
		*/
		bool					loadIndices(PlyFile * file);
//		bool					loadUVs(PlyFile * file);

		/*!
		 * @function createVertexAttribute
		 * @param[out] mem The vulkan memory allocated
		 * @param[in] mem_size The size of the vulkan memory allocated
		 * @param[out] buffer_index the index in the _buffers vector
		 */
		void					createVertexAttribute(VkDeviceMemory& mem, uint32_t mem_size, uint32_t *buffer_index);

		/*!
		* @function createIndexAttribute
		* @param[out] mem The vulkan memory allocated
		* @param[in] mem_size The size of the vulkan memory allocated
		* @param[out] buffer Vulkan buffer object
		*/
		void					createIndexAttribute(VkDeviceMemory& mem, uint32_t mem_size, VkBuffer& buffer);

		/*!
		 * @function updateVkAttribute
		 * @param[in] data Vcetor containing data
		 * @param[in] mem The vulkan memory to copy to
		 */
		void					updateVkAttribute(std::vector<float>& data, VkDeviceMemory& mem);

		/*!
		* @function updateVkAttribute
		* @warning Polymorphed to support integer type, used for indices
		* @param[in] data Vcetor containing data
		* @param[in] mem The vulkan memory to copy to
		*/
		void					updateVkAttribute(std::vector<int>& data, VkDeviceMemory& mem);

		/*!
		 * @funciton deleteVkAttrivute
		 * @param[in] buffer the buffer to be destroyed
		 * @param[in] mem the memory to be freed
		 */
		void					deleteVkAttribute(VkBuffer& buffer, VkDeviceMemory& mem);
	private:
		pv::PhysicalDevice&		_phys;
		pv::Device&				_device;

		std::vector<VkBuffer>		_buffers; //Attributes buffers
		std::vector<VkDeviceSize>	_offsets; //Offsets in buffers

		std::vector<float>		_vertex; //!< Vertex data
		uint32_t				_vertex_buffer_index; //!< Vulkan buffer index in _buffers vector
		VkDeviceMemory			_vertex_memory; //!< Vulkan vertex data
		uint32_t				_vertex_memory_size; //!< Vertex memory size in bytes

	public:
		std::vector<float>		_normal; //!< Normal data
	private:
		uint32_t				_normal_buffer_index; //!< Vulkan buffer index in _buffers vector
		VkDeviceMemory			_normal_memory; //!< Vulkan normal data
		uint32_t				_normal_memory_size; //!< Normal memory size in bytes

		
		VkBuffer				_index_input; //!< Vulkan index buffer
		std::vector<int>		_index; //!< Index data
		VkDeviceMemory			_index_memory; //!< Vulkan index data
		uint32_t				_index_memory_size; //!< Index space size in bytes
	};
}

#endif