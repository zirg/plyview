#pragma once

#ifndef PV_CONTEXT_H
#define PV_CONTEXT_H

#include <iostream>
#include <cstdint>
#include <vulkan/vulkan.h>
#include <vulkan/vk_sdk_platform.h>

#include "tools.h"

extern const char *g_instance_extensionNames[];
extern const char *g_device_extensionNames[];

namespace pv {
	/*!
	* @class Context
	* @brief The context class create a vulkan instance,
	* with required plateform dependent extensions 
	*/
	class Context
	{
	public:
		/*!
		 * @func Context
		 * @brief Default constructor
		 */
		Context();

		/*
		 * @func ~Context
		 * @brief Destructor
		 */
		~Context();

		VkInstance				vk_instance; //!< Accesible vulkan native instance type
	private:
	};
}



#endif