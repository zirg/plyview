#include "program.h"

namespace pv {
	Program::Program(pv::Device& device) 
		: _device(device) {
	}
	Program::~Program() {}
	
	void				Program::bind(VkCommandBuffer& cmd) {
		vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, vk_pipeline);
	}

	void				Program::createVkPipelineInfos() {
		std::memset(_dynamicStateEnables, 0, sizeof _dynamicStateEnables);
		std::memset(&_dynamicState, 0, sizeof _dynamicState);
		_dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
		_dynamicState.pDynamicStates = _dynamicStateEnables;

		std::memset(&_create_infos, 0, sizeof(_create_infos));
		_create_infos.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
		_create_infos.layout = vk_pipeline_layout;

		std::memset(&_vi, 0, sizeof(_vi));
		_vi.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

		std::memset(&_ia, 0, sizeof(_ia));
		_ia.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
		_ia.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;

		std::memset(&_rs, 0, sizeof(_rs));
		_rs.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
		_rs.polygonMode = VK_POLYGON_MODE_FILL;
		_rs.cullMode = VK_CULL_MODE_BACK_BIT;
		_rs.frontFace = VK_FRONT_FACE_CLOCKWISE;
		_rs.depthClampEnable = VK_FALSE;
		_rs.rasterizerDiscardEnable = VK_FALSE;
		_rs.depthBiasEnable = VK_FALSE;
		_rs.lineWidth = 1.0;

		std::memset(&_cb, 0, sizeof(_cb));
		_cb.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
		static VkPipelineColorBlendAttachmentState att_state[1];
		memset(att_state, 0, sizeof(att_state));
		att_state[0].colorWriteMask = 0xf;
		att_state[0].blendEnable = VK_FALSE;
		_cb.attachmentCount = 1;
		_cb.pAttachments = att_state;

		std::memset(&_vp, 0, sizeof(_vp));
		_vp.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
		_vp.viewportCount = 1;
		_dynamicStateEnables[_dynamicState.dynamicStateCount++] = VK_DYNAMIC_STATE_VIEWPORT;
		_vp.scissorCount = 1;
		_dynamicStateEnables[_dynamicState.dynamicStateCount++] = VK_DYNAMIC_STATE_SCISSOR;

		std::memset(&_ds, 0, sizeof(_ds));
		_ds.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
		_ds.depthTestEnable = VK_TRUE;
		_ds.depthWriteEnable = VK_TRUE;
		_ds.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
		_ds.depthBoundsTestEnable = VK_FALSE;
		_ds.back.failOp = VK_STENCIL_OP_KEEP;
		_ds.back.passOp = VK_STENCIL_OP_KEEP;
		_ds.back.compareOp = VK_COMPARE_OP_ALWAYS;
		_ds.stencilTestEnable = VK_FALSE;
		_ds.front = _ds.back;

		std::memset(&_ms, 0, sizeof(_ms));
		_ms.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
		_ms.pSampleMask = NULL;
		_ms.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	}

	bool		Program::createSpvShader(const std::string& filename, VkShaderModule *shader) {
		VkShaderModuleCreateInfo	shader_info;
		VkResult					r;
		void						*code = 0;
		int							size = 0;
		size_t						usize = 0;
		std::ifstream				file(filename, std::ifstream::in | std::ifstream::binary);

		PV_ASSERT(file.is_open() != false, filename.c_str());

		file.seekg(0, file.end);
		usize = file.tellg();
		size = file.tellg();
		file.seekg(0, file.beg);
		code = new char[size];
		file.read((char *)code, size);

		std::memset(&shader_info, 0, sizeof(VkShaderModuleCreateInfo));
		shader_info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		shader_info.pCode = (const uint32_t *)code;
		shader_info.codeSize = usize;

		r = vkCreateShaderModule(_device.vk_device, &shader_info, NULL, shader);
		PV_ASSERT_VK(r, VK_SUCCESS, "Create shader module KO");

		delete[] code;
		file.close();

		return true;
	}
}

namespace pv {
	FlatProgram::FlatProgram(pv::Device& device) 
		: Program::Program(device) {
	}
	
	FlatProgram::~FlatProgram() {
	}


	void									FlatProgram::createVkPipeline(VkRenderPass& renderpass, uint32_t subpass) {
		VkResult							r;
		VkShaderModule						vert_shader;
		VkShaderModule						frag_shader;
		VkPipelineShaderStageCreateInfo		shaderStages[2];
		VkPipelineCacheCreateInfo			cache_infos;

		createVkLayout();
		createVkPipelineInfos();
		createVkDecriptorPool();

		_create_infos.stageCount = 2;

		std::memset(&shaderStages, 0, 2 * sizeof(VkPipelineShaderStageCreateInfo));

		PV_ASSERT(createSpvShader(flat_vertex_shader, &vert_shader) != false, "Flat program vertex shader");
			
		shaderStages[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		shaderStages[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
		shaderStages[0].module = vert_shader;
		shaderStages[0].pName = "main";

		PV_ASSERT(createSpvShader(flat_fragment_shader, &frag_shader) != false, "Flat program fragment shader");

		shaderStages[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		shaderStages[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
		shaderStages[1].module = frag_shader;
		shaderStages[1].pName = "main";

		std::memset(&cache_infos, 0, sizeof(cache_infos));
		cache_infos.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;

		r = vkCreatePipelineCache(_device.vk_device, &cache_infos, NULL, &vk_pipeline_cache);
		PV_ASSERT_VK(r, VK_SUCCESS, "Creating pipeline cache failed");

		_create_infos.pVertexInputState = &_vi;
		_create_infos.pInputAssemblyState = &_ia;
		_create_infos.pRasterizationState = &_rs;
		_create_infos.pColorBlendState = &_cb;
		_create_infos.pMultisampleState = &_ms;
		_create_infos.pViewportState = &_vp;
		_create_infos.pDepthStencilState = &_ds;
		_create_infos.pStages = shaderStages;
		_create_infos.renderPass = renderpass;
		_create_infos.pDynamicState = &_dynamicState;

		r = vkCreateGraphicsPipelines(_device.vk_device, vk_pipeline_cache, 1, &_create_infos, NULL, &vk_pipeline);
		PV_ASSERT_VK(r, VK_SUCCESS, "Creating graphics pipeline failed");

		vkDestroyShaderModule(_device.vk_device, vert_shader, NULL);
		vkDestroyShaderModule(_device.vk_device, frag_shader, NULL);
	}
	void				FlatProgram::createVkPipelineInfos() {
		Program::createVkPipelineInfos();

		//layout (location = 0) in vec4 position
		std::memset(&_vi_bind, 0, sizeof(_vi_bind));
		_vi_bind[0].binding = 0;
		_vi_bind[0].stride = sizeof(float) * 4;
		_vi_bind[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
		std::memset(&_vi_attr, 0, sizeof(VkVertexInputAttributeDescription));
		_vi_attr[0].format = VkFormat::VK_FORMAT_R32G32B32A32_SFLOAT;
		_vi_attr[0].binding = 0;
		_vi_attr[0].location = 0;
		_vi_attr[0].offset = 0;

		//layout (location = 1) in vec4 normal
		_vi_bind[1].binding = 1;
		_vi_bind[1].stride = sizeof(float) * 4;
		_vi_bind[1].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
		_vi_attr[1].format = VkFormat::VK_FORMAT_R32G32B32A32_SFLOAT;
		_vi_attr[1].binding = 1;
		_vi_attr[1].location = 1;
		_vi_attr[1].offset = 0;

		//Setup vkGraphicsPipelineCreateInfos.vertexInputStateCreateInfos struct
		std::memset(&_vi, 0, sizeof(_vi));
		_vi.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
		_vi.vertexAttributeDescriptionCount = 2;
		_vi.pVertexAttributeDescriptions = _vi_attr;
		_vi.vertexBindingDescriptionCount = 2;
		_vi.pVertexBindingDescriptions = _vi_bind;
	}
	void									FlatProgram::createVkLayout() {
		VkResult							r;
		VkDescriptorSetLayoutBinding		layout_bindings[2];
		VkDescriptorSetLayoutCreateInfo		desc_layout_info;
		VkPipelineLayoutCreateInfo			pipe_layout_info;


		std::memset(layout_bindings, 0, sizeof(VkDescriptorSetLayoutBinding) * 1);
		layout_bindings[0].binding = 0;
		layout_bindings[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		layout_bindings[0].descriptorCount = 1;
		layout_bindings[0].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		layout_bindings[0].pImmutableSamplers = NULL;
		

		std::memset(&desc_layout_info, 0, sizeof(VkDescriptorSetLayoutCreateInfo));
		desc_layout_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		desc_layout_info.bindingCount = 1;
		desc_layout_info.pBindings = layout_bindings;
		r = vkCreateDescriptorSetLayout(_device.vk_device, &desc_layout_info, NULL, &vk_desc_layout);
		PV_ASSERT_VK(r, VK_SUCCESS, "Create descriptor set layout failed");

		std::memset(&pipe_layout_info, 0, sizeof(VkPipelineLayoutCreateInfo));
		pipe_layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		pipe_layout_info.pNext = NULL;
		pipe_layout_info.setLayoutCount = 1;
		pipe_layout_info.pSetLayouts = &vk_desc_layout;
		r = vkCreatePipelineLayout(_device.vk_device, &pipe_layout_info, NULL, &vk_pipeline_layout);
		PV_ASSERT_VK(r, VK_SUCCESS, "Create pipeline layout failed");
	}

	void							FlatProgram::createVkDecriptorPool() {
		VkResult					r;
		VkDescriptorPoolSize		pool_types[1];
		VkDescriptorPoolCreateInfo	pool_info;

		pool_types[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		pool_types[0].descriptorCount = 1;

		std::memset(&pool_info, 0, sizeof(VkDescriptorPoolCreateInfo));
		pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
		pool_info.poolSizeCount = 1;
		pool_info.maxSets = 1;
		pool_info.pPoolSizes = pool_types;
		r = vkCreateDescriptorPool(_device.vk_device, &pool_info, NULL, &vk_desc_pool);
		PV_ASSERT_VK(r, VK_SUCCESS, "Descriptor pool creation failed");
	}

	std::string		flat_fragment_shader;
	std::string		flat_vertex_shader;

}