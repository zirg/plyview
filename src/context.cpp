#include "context.h"

#ifdef _WIN32
const char *g_instance_extensionNames[] = { VK_KHR_SURFACE_EXTENSION_NAME, VK_KHR_WIN32_SURFACE_EXTENSION_NAME };
#else
const char *g_wsi_extensionNames[] = { VK_KHR_SURFACE_EXTENSION_NAME, VK_KHR_XCB_SURFACE_EXTENSION_NAME };
#endif
const char *g_device_extensionNames[] = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };

namespace pv {
	Context::Context() {
		VkResult					err;
		VkInstanceCreateInfo		infos;

		std::memset(&infos, 0, sizeof(VkInstanceCreateInfo));
		infos.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		infos.pApplicationInfo = NULL;
		infos.enabledExtensionCount = 2;
		infos.ppEnabledExtensionNames = g_instance_extensionNames;

		err = vkCreateInstance(&infos, NULL, &vk_instance);
		PV_ASSERT_VK(err, VK_SUCCESS, "pv::Context Can't initialize vulkan")
	}

	Context::~Context() {}
}
