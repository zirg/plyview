#include "window.h"

#ifdef _WIN32
HINSTANCE					g_connection;
#else
xcb_connection_t			*g_connection;
xcb_screen_t				*g_screen;
#endif

namespace pv {

	WindowKHR::WindowKHR(pv::Context& context, pv::PhysicalDevice& physical_device) : _ctx(context), _phys(physical_device) {
		width = 1280;
		height = 720;

		GET_INSTANCE_PROC_ADDR(_ctx.vk_instance, GetPhysicalDeviceSurfaceSupportKHR);
		GET_INSTANCE_PROC_ADDR(_ctx.vk_instance, GetPhysicalDeviceSurfaceCapabilitiesKHR);
		GET_INSTANCE_PROC_ADDR(_ctx.vk_instance, GetPhysicalDeviceSurfaceFormatsKHR);
		GET_INSTANCE_PROC_ADDR(_ctx.vk_instance, GetPhysicalDeviceSurfacePresentModesKHR);

		initWindow();
		initSurface();
		initFormatAndColor();
	}

	WindowKHR::WindowKHR(pv::Context& context, pv::PhysicalDevice& physical_device, int pWidth, int pHeight) 
		: _ctx(context), _phys(physical_device), width(pWidth), height(pHeight) {

		GET_INSTANCE_PROC_ADDR(_ctx.vk_instance, GetPhysicalDeviceSurfaceSupportKHR);
		GET_INSTANCE_PROC_ADDR(_ctx.vk_instance, GetPhysicalDeviceSurfaceCapabilitiesKHR);
		GET_INSTANCE_PROC_ADDR(_ctx.vk_instance, GetPhysicalDeviceSurfaceFormatsKHR);
		GET_INSTANCE_PROC_ADDR(_ctx.vk_instance, GetPhysicalDeviceSurfacePresentModesKHR);

		initWindow();
		initSurface();
		initFormatAndColor();
	}

	WindowKHR::~WindowKHR() {}

#ifdef _WIN32
	bool		WindowKHR::initWindow()
	{
		WNDCLASSEX win_class;

		// Initialize the window class structure:
		win_class.cbSize = sizeof(WNDCLASSEX);
		win_class.style = CS_HREDRAW | CS_VREDRAW;
		win_class.lpfnWndProc = WndProc;
		win_class.cbClsExtra = 0;
		win_class.cbWndExtra = 0;
		win_class.hInstance = g_connection; // hInstance
		win_class.hIcon = LoadIcon(NULL, IDI_APPLICATION);
		win_class.hCursor = LoadCursor(NULL, IDC_ARROW);
		win_class.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
		win_class.lpszMenuName = NULL;
		win_class.lpszClassName = "plyViewer";
		win_class.hIconSm = LoadIcon(NULL, IDI_WINLOGO);
		// Register window class:
		if (!RegisterClassEx(&win_class)) {
			// It didn't work, so try to give a useful error:
			std::cerr << "Unexpected error trying to start the application!\n" << std::endl;
			return false;
		}
		// Create window with the registered class:
		RECT wr = { 0, 0, width, height };
		AdjustWindowRect(&wr, WS_OVERLAPPEDWINDOW, FALSE);
		window = CreateWindowEx(0,
			"plyViewer",           // class name
			"plyViewer",           // app name
			WS_OVERLAPPEDWINDOW | // window style
			WS_VISIBLE | WS_SYSMENU,
			100, 100,           // x/y coords
			wr.right - wr.left, // width
			wr.bottom - wr.top, // height
			NULL,               // handle to parent
			NULL,               // handle to menu
			g_connection,   // hInstance
			NULL);              // no extra parameters
		if (window == 0) {
			std::cerr << "Cannot create a window in which to draw!\n" << std::endl;
			return false;
		}
		return true;
	}

	bool		WindowKHR::initSurface()
	{
		VkResult					r;
		VkWin32SurfaceCreateInfoKHR	createInfo;

		createInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
		createInfo.pNext = NULL;
		createInfo.flags = 0;
		createInfo.hinstance = g_connection;
		createInfo.hwnd = window;

		r = vkCreateWin32SurfaceKHR(_ctx.vk_instance, &createInfo, NULL, &vk_surface);
		PV_ASSERT_VK(r, VK_SUCCESS, "pv::WindowKHR Create Win32 surface KHR failed")
			return true;
	}

#else
	bool		WindowKHR::initWindow() {
		uint32_t value_mask, value_list[32];

		window = xcb_generate_id(g_connection);

		value_mask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
		value_list[0] = g_screen->black_pixel;
		value_list[1] = XCB_EVENT_MASK_KEY_RELEASE | XCB_EVENT_MASK_EXPOSURE |
			XCB_EVENT_MASK_STRUCTURE_NOTIFY;

		xcb_create_window(g_connection, XCB_COPY_FROM_PARENT, window,
			_screen->root, 0, 0, 800, 600, 0,
			XCB_WINDOW_CLASS_INPUT_OUTPUT, g_screen->root_visual,
			value_mask, value_list);

		/* Magic code that will send notification when window is destroyed */
		xcb_intern_atom_cookie_t cookie =
			xcb_intern_atom(g_connection, 1, 12, "WM_PROTOCOLS");
		xcb_intern_atom_reply_t *reply =
			xcb_intern_atom_reply(g_connection, cookie, 0);

		xcb_intern_atom_cookie_t cookie2 =
			xcb_intern_atom(g_connection, 0, 16, "WM_DELETE_WINDOW");
		atom_wm_delete_window =
			xcb_intern_atom_reply(g_connection, cookie2, 0);

		xcb_change_propertyg_connection, XCB_PROP_MODE_REPLACE, window,
			(*reply).atom, 4, 32, 1,
			&(*atom_wm_delete_window).atom);
			free(reply);

			xcb_map_window(g_connection, window);
			return false;
	}

	bool		WindowKHR::initSurface()
	{
		VkResult err;

		VkXcbSurfaceCreateInfoKHR createInfo;
		createInfo.sType = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR;
		createInfo.pNext = NULL;
		createInfo.flags = 0;
		createInfo.connection = g_connection;
		createInfo.window = window;

		err = vkCreateXcbSurfaceKHR(ctx.vk_instance, &createInfo, NULL, &vk_surface);
		PV_ASSERT_VK(r, VK_SUCCESS, "pv::WindowKHR Create XCB surface KHR failed")
			return true;
	}
#endif

	bool		WindowKHR::initFormatAndColor() {
		uint32_t					formatCount = 0;
		VkSurfaceFormatKHR			*surfFormats = 0;

		if (fpGetPhysicalDeviceSurfaceFormatsKHR(_phys.vk_physical_device, vk_surface, &formatCount, NULL) != VK_SUCCESS) {
			std::cerr << "Get surface format count failed" << std::endl;
			return false;
		}
		std::cout << "Has formats : " << formatCount << std::endl;
		surfFormats = new VkSurfaceFormatKHR[formatCount];
		if (fpGetPhysicalDeviceSurfaceFormatsKHR(_phys.vk_physical_device, vk_surface, &formatCount, surfFormats) != VK_SUCCESS) {
			std::cerr << "Get surface formats failed" << std::endl;
			return false;
		}
		// If the format list includes just one entry of VK_FORMAT_UNDEFINED,
		// the surface has no preferred format.  Otherwise, at least one
		// supported format will be returned.
		if (formatCount == 1 && surfFormats[0].format == VK_FORMAT_UNDEFINED) {
			vk_format = VK_FORMAT_B8G8R8A8_UNORM;
		}
		else {
			vk_format = surfFormats[0].format;
		}
		vk_color_space = surfFormats[0].colorSpace;
		std::cout << "Format = " << vk_format << "   Color space = " << vk_color_space << std::endl;
		return true;
	}

}