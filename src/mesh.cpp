#include "mesh.h"

namespace pv {
	Mesh::Mesh(pv::PhysicalDevice& phys, pv::Device& device) 
		: _phys(phys), _device(device) {
	}

	Mesh::~Mesh() {
		_offsets.clear();
		_buffers.clear();
	}
	
	bool							Mesh::loadPlyFile(const std::string& filename) {
		int							nelems;
		char						**elem_names;
		int							file_type;
		float						version;
		PlyFile						*file;

		file = ply_open_for_reading(const_cast<char *>(filename.c_str()), &nelems, &elem_names, &file_type, &version);
		if (file == NULL) {
			std::cerr << "Cant open ply : " << filename << std::endl;
			return false;
		}
		std::cout << "Ply file elements: " << std::endl;
		for (int i = 0; i < nelems; i++) {
			std::cout << elem_names[i] << std::endl;
		}
		
		_vertex.clear();
		loadVertices(file);
		_vertex_memory_size = _vertex.size() * sizeof(float);
		createVertexAttribute(_vertex_memory, _vertex_memory_size, &_vertex_buffer_index);
		updateVkAttribute(_vertex, _vertex_memory);

		_index.clear();
		loadIndices(file);
		_index_memory_size = _index.size() * sizeof(int);
		createIndexAttribute(_index_memory, _index_memory_size, _index_input);
		updateVkAttribute(_index, _index_memory);


		_normal.clear();
		if (loadNormals(file) == false)
			generateNormals();
		_normal_memory_size = _normal.size() * sizeof(float);
		createVertexAttribute(_normal_memory, _normal_memory_size, &_normal_buffer_index);
		updateVkAttribute(_normal, _normal_memory);

		ply_close(file);
		return true;
	}

	bool					Mesh::loadVertices(PlyFile * file) {
		int					n_current_prop;
		int					n_current_elem;
		PlyProperty			**prop_list;
		ply_vertex_t		vertex;

		prop_list = ply_get_element_description(file, "vertex", &n_current_elem, &n_current_prop);
		if (prop_list == NULL) {
			std::cerr << "ply file doesnt contains vertex element" << std::endl;
			return false;
		}
		std::cout << "Vertex properties: " << " nVertex =" << n_current_elem << std::endl;
		for (int i = 0; i < n_current_prop; i++) {
			prop_list[i]->internal_type = PLY_FLOAT;
			prop_list[i]->offset = i * sizeof(float);
			std::cout << "Name: " << prop_list[i]->name << std::endl;
			std::cout << "Internal Type: " << prop_list[i]->internal_type << std::endl;
			std::cout << "External Type: " << prop_list[i]->external_type << std::endl;
			std::cout << "is list: " << prop_list[i]->is_list << std::endl;
			std::cout << "count external: " << prop_list[i]->count_external << std::endl;
			std::cout << "count internal: " << prop_list[i]->count_internal << std::endl;
			std::cout << "Offset: " << prop_list[i]->offset << std::endl;
			ply_get_property(file, "vertex", prop_list[i]);
		}

		for (int i = 0; i < n_current_elem; i++) {
			ply_get_element(file, &vertex);
			_vertex.push_back(vertex.x);
			_vertex.push_back(vertex.y);
			_vertex.push_back(vertex.z);
			_vertex.push_back(1.0f);
		}
		return true;
	}

	void								Mesh::generateNormals() {
		std::multimap<int, float*>		v_normals;
		vec3							v, w;
		vec3							normal;

		//Reserve normals storage
		_normal.resize(_vertex.size(), 1.0);
		std::cout << "Gen nromals" << _vertex.size() << std::endl;

		//Compute normal for each face
		for (unsigned int i = 0; i < _index.size(); i += 3) { //Polygons should be triangles
			vec3_sub(v, &_vertex.data()[_index[i] * 4], &_vertex.data()[_index[i + 2] * 4]);
			vec3_sub(w, &_vertex.data()[_index[i + 1] * 4], &_vertex.data()[_index[i + 2] * 4]);
			vec3_mul_cross(normal, v, w);
			float * ins = new float[3];
			std::memcpy(ins, normal, 3 * sizeof(float));
			v_normals.insert(std::make_pair(_index[i], ins));
			ins = new float[3];
			std::memcpy(ins, normal, 3 * sizeof(float));
			v_normals.insert(std::make_pair(_index[i + 1], ins));
			ins = new float[3];
			std::memcpy(ins, normal, 3 * sizeof(float));
			v_normals.insert(std::make_pair(_index[i + 2], ins));
		}
		std::cout << "Gen nromals" << _normal.size() << std::endl;

		//Average normal for common vertices
		for (unsigned int i = 0; i < _vertex.size(); i += 4) {
			vec3	normal_avg;
			int		v_number = i / 4;
			float	v_usages = (float)v_normals.count(v_number);
			std::multimap<int, float *>::iterator norms;

			std::memset(normal_avg, 0, sizeof(normal_avg));
			while (v_usages > 0 && (norms = v_normals.find(v_number)) != v_normals.end()) {
				normal_avg[0] += norms->second[0] / v_usages;
				normal_avg[1] += norms->second[1] / v_usages;
				normal_avg[2] += norms->second[2] / v_usages;
				delete [] norms->second;
				v_normals.erase(norms);
				
			}
			_normal[i] = normal_avg[0];
			_normal[i + 1] = normal_avg[1];
			_normal[i + 2] = normal_avg[2];
			_normal[i + 3] = 1.0;
		}
		std::cout << "Gen nromals" << _normal.size() << std::endl;
	}
	
	bool					Mesh::loadNormals(PlyFile * file) {
		int					n_current_prop;
		int					n_current_elem;
		PlyProperty			**prop_list;
		ply_vertex_t		n;

		prop_list = ply_get_element_description(file, "normal", &n_current_elem, &n_current_prop);
		if (prop_list == NULL) {
			std::cerr << "ply file doesnt contains normal element" << std::endl;
			return false;
		}
		std::cout << "Normal properties: " << " nNormals =" << n_current_elem << std::endl;
		for (int i = 0; i < n_current_prop; i++) {
			prop_list[i]->internal_type = PLY_FLOAT;
			prop_list[i]->offset = i * sizeof(float);
			std::cout << "Name: " << prop_list[i]->name << std::endl;
			std::cout << "Internal Type: " << prop_list[i]->internal_type << std::endl;
			std::cout << "External Type: " << prop_list[i]->external_type << std::endl;
			std::cout << "is list: " << prop_list[i]->is_list << std::endl;
			std::cout << "count external: " << prop_list[i]->count_external << std::endl;
			std::cout << "count internal: " << prop_list[i]->count_internal << std::endl;
			std::cout << "Offset: " << prop_list[i]->offset << std::endl;
			ply_get_property(file, "normal", prop_list[i]);
		}

		for (int i = 0; i < n_current_elem; i++) {
			ply_get_element(file, &n);
			_normal.push_back(n.x);
			_normal.push_back(n.y);
			_normal.push_back(n.z);
		}
		return true;
	}

	bool					Mesh::loadIndices(PlyFile * file) {
		int					n_current_prop;
		int					n_current_elem;
		PlyProperty			**prop_list;
		ply_face_t			face;

		prop_list = ply_get_element_description(file, "face", &n_current_elem, &n_current_prop);
		if (prop_list == NULL) {
			std::cerr << "ply file doesnt contains vertex element" << std::endl;
			return false;
		}
		std::cout << "Face properties: " << " nFaces =" << n_current_elem << std::endl;
		for (int i = 0; i < n_current_prop; i++) {
			prop_list[i]->internal_type = PLY_INT;
			prop_list[i]->offset = offsetof(ply_face_t, indices);
			prop_list[i]->count_internal = prop_list[i]->count_external;
			prop_list[i]->count_offset = offsetof(ply_face_t, n_indices);
			std::cout << "Name: " << prop_list[i]->name << std::endl;
			std::cout << "Internal Type: " << prop_list[i]->internal_type << std::endl;
			std::cout << "External Type: " << prop_list[i]->external_type << std::endl;
			std::cout << "is list: " << prop_list[i]->is_list << std::endl;
			std::cout << "count external: " << prop_list[i]->count_external << std::endl;
			std::cout << "count internal: " << prop_list[i]->count_internal << std::endl;
			std::cout << "Offset: " << prop_list[i]->offset << std::endl;
			std::cout << "Count Offset: " << prop_list[i]->count_offset << std::endl;
			ply_get_property(file, "face", prop_list[i]);
		}

		for (int i = 0; i < n_current_elem; i++) {
			ply_get_element(file, &face);
			for (int j = 0; j < face.n_indices; j++) {//Can push N vertex polys
				_index.push_back(face.indices[j]);//But this is used as triangles
			}
			delete face.indices;
		}
		return true;
	}

	uint32_t				Mesh::vk_buffer_count() const {
		return _buffers.size();
	}

	VkBuffer*				Mesh::vk_buffers() {
		return const_cast<VkBuffer *>(_buffers.data()); //const_cast vector data accessor to simplify storage
	}

	VkDeviceSize*			Mesh::vk_offsets() {
		return const_cast<VkDeviceSize *>(_offsets.data()); //const_cast vector data accessor to simplify storage
	}

	VkBuffer&				Mesh::vk_indices() {
		return _index_input;
	}

	uint32_t				Mesh::vk_indices_count() const {
		return _index.size();
	}

	void							Mesh::createVertexAttribute(VkDeviceMemory& mem, uint32_t mem_size, uint32_t *buffer_index) {
		VkResult					r;
		VkBuffer					buffer;
		VkBufferCreateInfo			buff_info;
		VkMemoryRequirements		mem_reqs;
		VkMemoryAllocateInfo		mem_info;

		std::memset(&buff_info, 0, sizeof(VkBufferCreateInfo));
		buff_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		buff_info.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
		buff_info.size = mem_size;
		r = vkCreateBuffer(_device.vk_device, &buff_info, NULL, &buffer);
		PV_ASSERT_VK(r, VK_SUCCESS, "Creation vk buffer failed");

		vkGetBufferMemoryRequirements(_device.vk_device, buffer, &mem_reqs);
		std::memset(&mem_info, 0, sizeof(VkMemoryAllocateInfo));
		mem_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		mem_info.memoryTypeIndex = _phys.getMemoryIndex(mem_reqs.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
		mem_info.allocationSize = mem_reqs.size;
		r = vkAllocateMemory(_device.vk_device, &mem_info, NULL, &mem);
		PV_ASSERT_VK(r, VK_SUCCESS, "Buffer mem allocation failed");

		r = vkBindBufferMemory(_device.vk_device, buffer, mem, 0);
		PV_ASSERT_VK(r, VK_SUCCESS, "Bind memory to buffer fail");

		*buffer_index = _buffers.size();
		_buffers.push_back(buffer);
		_offsets.push_back(0);
	}

	void							Mesh::createIndexAttribute(VkDeviceMemory& mem, uint32_t mem_size, VkBuffer& buffer) {
		VkResult					r;
		VkBufferCreateInfo			buff_info;
		VkMemoryRequirements		mem_reqs;
		VkMemoryAllocateInfo		mem_info;

		std::memset(&buff_info, 0, sizeof(VkBufferCreateInfo));
		buff_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		buff_info.usage = VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
		buff_info.size = mem_size;
		r = vkCreateBuffer(_device.vk_device, &buff_info, NULL, &buffer);
		PV_ASSERT_VK(r, VK_SUCCESS, "Creation vk buffer failed");

		vkGetBufferMemoryRequirements(_device.vk_device, buffer, &mem_reqs);
		std::memset(&mem_info, 0, sizeof(VkMemoryAllocateInfo));
		mem_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		mem_info.memoryTypeIndex = _phys.getMemoryIndex(mem_reqs.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
		mem_info.allocationSize = mem_reqs.size;
		r = vkAllocateMemory(_device.vk_device, &mem_info, NULL, &mem);
		PV_ASSERT_VK(r, VK_SUCCESS, "Buffer mem allocation failed");

		r = vkBindBufferMemory(_device.vk_device, buffer, mem, 0);
		PV_ASSERT_VK(r, VK_SUCCESS, "Bind memory to buffer fail");

		
	}

	void					Mesh::updateVkAttribute(std::vector<float>& data, VkDeviceMemory& mem) {
		VkResult			r;
		float				*pData;

		r = vkMapMemory(_device.vk_device, mem, 0, data.size() * sizeof(float), 0, (void **)&pData);
		PV_ASSERT_VK(r, VK_SUCCESS, "Can't map buffer memory");

		std::memcpy(pData, data.data(), data.size() * sizeof(float));
		vkUnmapMemory(_device.vk_device, mem);
	}

	void					Mesh::updateVkAttribute(std::vector<int>& data, VkDeviceMemory& mem) {
		VkResult			r;
		uint32_t			*pData;

		r = vkMapMemory(_device.vk_device, mem, 0, data.size() * sizeof(int), 0, (void **)&pData);
		PV_ASSERT_VK(r, VK_SUCCESS, "Can't map buffer memory");

		std::memcpy(pData, data.data(), data.size() * sizeof(float));
		vkUnmapMemory(_device.vk_device, mem);
	}

	void					Mesh::deleteVkAttribute(VkBuffer& buffer, VkDeviceMemory& mem) {
		vkFreeMemory(_device.vk_device, mem, NULL);
		vkDestroyBuffer(_device.vk_device, buffer, NULL);
	}
}