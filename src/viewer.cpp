#include "viewer.h"

namespace pv {

	Viewer::Viewer() {
		_ctx = 0;
		_phys = 0;
		_window = 0;
		_device = 0;
		_swapchain = 0;
		_mesh = 0;
		_render = 0;
	}

	Viewer::~Viewer() {
		delete _swapchain;
		delete _render;
		delete _mesh;
		delete _device;
		delete _window;
		delete _phys;
		delete _ctx;
	}

	bool			Viewer::init(char *plyPath) {

		_ctx = new pv::Context();
		_phys = new pv::PhysicalDevice(*_ctx);
		_window = new pv::WindowKHR(*_ctx, *_phys);
		_device = new pv::Device(*_phys, *_window);

		_mesh = new pv::Mesh(*_phys, *_device);
		if (_mesh->loadPlyFile(plyPath) == false)
			return false;

		_render = new pv::FlatRenderer(*_device, *_window);
		_swapchain = new pv::SwapChain(*_phys, *_window, *_device, *_render);



		{
			VkMemoryRequirements		mem_reqs;
			VkBufferCreateInfo			uniform_buf_info;
			VkMemoryAllocateInfo		uniform_buf_mem_info;

			/* UNIFORM */
			std::memset(&uniform_buf_info, 0, sizeof(VkBufferCreateInfo));
			uniform_buf_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
			uniform_buf_info.size = sizeof(uniformbuf_t);
			uniform_buf_info.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
			if (vkCreateBuffer(_device->vk_device, &uniform_buf_info, NULL, &_uniform_buf) != VK_SUCCESS) {
				std::cerr << "Creation uniform buffer failed" << std::endl;
				return false;
			}
			vkGetBufferMemoryRequirements(_device->vk_device, _uniform_buf, &mem_reqs);
			std::memset(&uniform_buf_mem_info, 0, sizeof(VkMemoryAllocateInfo));
			uniform_buf_mem_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
			uniform_buf_mem_info.memoryTypeIndex = _phys->getMemoryIndex(mem_reqs.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
			uniform_buf_mem_info.allocationSize = mem_reqs.size;
			if (vkAllocateMemory(_device->vk_device, &uniform_buf_mem_info, NULL, &_uniform_buf_mem) != VK_SUCCESS) {
				std::cerr << "Uniform buffer mem alloc failed" << std::endl;
				return false;
			}
			if (vkBindBufferMemory(_device->vk_device, _uniform_buf, _uniform_buf_mem, 0) != VK_SUCCESS) {
				std::cerr << "Bind uniform memory to buffer fail" << std::endl;
				return false;
			}
			_uniform_desc_info.buffer = _uniform_buf;
			_uniform_desc_info.offset = 0;
			_uniform_desc_info.range = sizeof(uniformbuf_t);
			/* END UNIFORM */
		}
		{
			VkWriteDescriptorSet			desc_wr[1];
			VkDescriptorSetAllocateInfo		desc_set_alloc;

			std::memset(&desc_set_alloc, 0, sizeof(VkDescriptorSetAllocateInfo));
			desc_set_alloc.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
			desc_set_alloc.descriptorPool = _render->pv_programs[0].vk_desc_pool;
			desc_set_alloc.descriptorSetCount = 1;
			desc_set_alloc.pSetLayouts = &_render->pv_programs[0].vk_desc_layout;
			if (vkAllocateDescriptorSets(_device->vk_device, &desc_set_alloc, &_desc_set) != VK_SUCCESS) {
				std::cerr << "Descriptor set allocaiton failed" << std::endl;
				return false;
			}


			std::memset(&desc_wr[0], 0, sizeof(VkWriteDescriptorSet));
			desc_wr[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			desc_wr[0].dstSet = _desc_set;
			desc_wr[0].descriptorCount = 1;
			desc_wr[0].descriptorType = VkDescriptorType::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			desc_wr[0].pBufferInfo = &_uniform_desc_info;

			vkUpdateDescriptorSets(_device->vk_device, 1, desc_wr, 0, NULL);
		}
		return pvCreateDrawCmd();
	}

	void			Viewer::run() {
#ifdef _WIN32
		MSG		msg;

		while (1) {
			PeekMessage(&msg, NULL, 0, 0, PM_REMOVE);
			if (msg.message == WM_QUIT) // check for a quit message
				return;
			else {
				/* Translate and dispatch to event queue*/
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			RedrawWindow(_window->window, NULL, NULL, RDW_INTERNALPAINT);
		}
#endif
	}

	bool		Viewer::pvBeginCommand(uint32_t frame) { 
		VkCommandBufferInheritanceInfo		cmd_hinfo;
		VkCommandBufferBeginInfo			cmd_info;

		std::memset(&cmd_hinfo, 0, sizeof(VkCommandBufferInheritanceInfo));
		cmd_hinfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
		cmd_hinfo.renderPass = VK_NULL_HANDLE;
		cmd_hinfo.subpass = 0;
		cmd_hinfo.framebuffer = VK_NULL_HANDLE;
		cmd_hinfo.occlusionQueryEnable = VK_FALSE;
		cmd_hinfo.queryFlags = 0;
		cmd_hinfo.pipelineStatistics = 0;

		std::memset(&cmd_info, 0, sizeof(VkCommandBufferBeginInfo));
		cmd_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		cmd_info.pInheritanceInfo = &cmd_hinfo;
		if (vkBeginCommandBuffer(_swapchain->vk_commands[frame], &cmd_info) != VK_SUCCESS) {
			std::cerr << "Begin command buffer fails" << std::endl;
			return false;
		}
		return true;
	}

	bool		Viewer::pvEndCommand(uint32_t frame) {
		vkEndCommandBuffer(_swapchain->vk_commands[frame]);
		return true;
	}

	bool		Viewer::pvCreateDrawCmd() {
		VkImageMemoryBarrier	prePresentBarrier;


		std::memset(&prePresentBarrier, 0, sizeof(prePresentBarrier));
		prePresentBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		prePresentBarrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		prePresentBarrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
		prePresentBarrier.oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		prePresentBarrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
		prePresentBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		prePresentBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		prePresentBarrier.subresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };


		for (uint32_t i = 0; i < _swapchain->getSwapChainImageCount(); i++) {


			if (pvBeginCommand(i) == false)
				return false;

			_render->renderToCmd(_swapchain->vk_commands[i], _swapchain->vk_framebuffers[i], *_mesh, _desc_set);

			prePresentBarrier.image = _swapchain->vk_images[i];
			vkCmdPipelineBarrier(_swapchain->vk_commands[i], VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
				VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, NULL, 0,
				NULL, 1, &prePresentBarrier);

			pvEndCommand(i);
		}
		return true;
	}

	bool		Viewer::pvResize(uint32_t width, uint32_t height) {
		_window->width = width;
		_window->height = height;
		return true;
	}

	bool		Viewer::pvClose(uint32_t exit_status) {
		(void)exit_status;
		return true;
	}

	bool		Viewer::pvDraw() { 
		uint32_t					g_frame = 0;
		VkSemaphore					presentCompleteSemaphore;
		VkSemaphoreCreateInfo		presentCompleteSemaphoreCreateInfo;
		VkFence						nullFence = VK_NULL_HANDLE;
		uint32_t					fam_idx;

		fam_idx = _device->getQueueFamilyIndex(true, VK_QUEUE_GRAPHICS_BIT);
		std::memset(&presentCompleteSemaphoreCreateInfo, 0, sizeof(VkSemaphoreCreateInfo));
		presentCompleteSemaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
		presentCompleteSemaphoreCreateInfo.pNext = NULL;
		presentCompleteSemaphoreCreateInfo.flags = 0;

		_device->waitIdle();

		if (vkCreateSemaphore(_device->vk_device, &presentCompleteSemaphoreCreateInfo, NULL, &presentCompleteSemaphore) != VK_SUCCESS) {
			std::cerr << "Can't create draw semaphore" << std::endl;
			return false;
		}

		// Get the index of the next available swapchain image:
		if (_device->fpAcquireNextImageKHR(_device->vk_device, _swapchain->vk_swapchain, UINT64_MAX, presentCompleteSemaphore, nullFence, &g_frame) != VK_SUCCESS) {
			std::cerr << "Acquire next image fails" << std::endl;
			return false;
		}

		if (_device->initImageLayout(_swapchain->vk_images[g_frame], VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, 0, VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, VK_IMAGE_ASPECT_COLOR_BIT) == false) {
			std::cerr << "Init image layout from color to present before submit fails" << std::endl;
			return false;
		}

		// Wait for the present complete semaphore to be signaled
		VkPipelineStageFlags pipe_stage_flags = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
		VkSubmitInfo submit_info;
		submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submit_info.pNext = NULL;
		submit_info.waitSemaphoreCount = 1;
		submit_info.pWaitSemaphores = &presentCompleteSemaphore;
		submit_info.pWaitDstStageMask = &pipe_stage_flags;
		submit_info.commandBufferCount = 1;
		submit_info.pCommandBuffers = &(_swapchain->vk_commands[g_frame]);
		submit_info.signalSemaphoreCount = 0;
		submit_info.pSignalSemaphores = NULL;

		if (vkQueueSubmit(_device->getQueue(fam_idx, 0), 1, &submit_info, nullFence) != VK_SUCCESS) {
			std::cerr << "Draw submit failed " << std::endl;
			return false;
		}
		VkPresentInfoKHR present;

		std::memset(&present, 0, sizeof(VkPresentInfoKHR));
		present.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		present.pNext = NULL;
		present.swapchainCount = 1;
		present.pSwapchains = &_swapchain->vk_swapchain;
		present.pImageIndices = &g_frame;

		if (_device->fpQueuePresentKHR(_device->getQueue(fam_idx, 0), &present) != VK_SUCCESS) {
			std::cerr << "Draw present failed" << std::endl;
			return false;
		}
		if (vkQueueWaitIdle(_device->getQueue(fam_idx, 0)) != VK_SUCCESS) {
			std::cerr << "Idle wait failed" << std::endl;
			return false;
		}

		vkDestroySemaphore(_device->vk_device, presentCompleteSemaphore, NULL);
		return true;
	}

	bool		Viewer::pvRotate(uniformbuf_t& uni) {
		// Put uniform into buffer
		float *pData;
		vkMapMemory(_device->vk_device, _uniform_buf_mem, 0, sizeof(uniformbuf_t), 0, (void **)&pData);
		std::memcpy(pData, &uni, sizeof(uni));
		vkUnmapMemory(_device->vk_device, _uniform_buf_mem);
		// End unfiorm sync
		return true;
	}

}