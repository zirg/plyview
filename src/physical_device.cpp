#include "physical_device.h"

namespace pv {

	PhysicalDevice::PhysicalDevice(Context& ctx) {
		VkResult		r;

		r = vkEnumeratePhysicalDevices(ctx.vk_instance, &_device_count, NULL);
		PV_ASSERT_VK(r, VK_SUCCESS, "pv::PhysicalDevice Can't count physical devices")
		
		PV_ASSERT(_device_count > 0, "No vulkan compatible device found !")

		_devices = new VkPhysicalDevice[_device_count];
		r = vkEnumeratePhysicalDevices(ctx.vk_instance, &_device_count, _devices);
		PV_ASSERT_VK(r, VK_SUCCESS, "pv::PhysicalDevice Enumerate physical devices failed")

		_props = new VkPhysicalDeviceProperties[_device_count];
		_mem_props = new VkPhysicalDeviceMemoryProperties[_device_count];
		std::cout << "Listing physical devices :" << std::endl;
		for (uint32_t i = 0; i < _device_count; i++) {
			std::cout << "=================" << std::endl;
			vkGetPhysicalDeviceProperties(_devices[i], &(_props[i]));
			std::cout << "Device " << _props[i].deviceName << std::endl;
			std::cout << "ID " << _props[i].deviceID << std::endl;
			std::cout << "Api v:" << _props[i].apiVersion << std::endl << std::endl;
			vkGetPhysicalDeviceMemoryProperties(_devices[i], &_mem_props[i]);
			for (uint32_t j = 0; j < _mem_props[i].memoryTypeCount; j++) {
				std::cout << "\tMemory :" << std::endl;
				std::cout << "\tVK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT : " << ((_mem_props[i].memoryTypes[j].propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) == VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) << std::endl;
				std::cout << "\tVK_MEMORY_PROPERTY_HOST_VISIBLE_BIT : " << ((_mem_props[i].memoryTypes[j].propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) == VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) << std::endl;
				std::cout << "\tVK_MEMORY_PROPERTY_HOST_COHERENT_BIT : " << ((_mem_props[i].memoryTypes[j].propertyFlags & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT) == VK_MEMORY_PROPERTY_HOST_COHERENT_BIT) << std::endl;
				std::cout << "\tVK_MEMORY_PROPERTY_HOST_CACHED_BIT : " << ((_mem_props[i].memoryTypes[j].propertyFlags & VK_MEMORY_PROPERTY_HOST_CACHED_BIT) == VK_MEMORY_PROPERTY_HOST_CACHED_BIT) << std::endl;
				std::cout << "\tVK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT : " << ((_mem_props[i].memoryTypes[j].propertyFlags & VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT) == VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT) << std::endl;
			}
			std::cout << "=================" << std::endl << std::endl;
		}

		vk_physical_device = _devices[0];
		vk_physical_device_props = _props[0];
		vk_physical_device_mem_props = _mem_props[0];
	}
	PhysicalDevice::~PhysicalDevice() {}

	int		PhysicalDevice::getMemoryIndex(uint32_t typeBits, uint32_t requirementsBits) {
		for (uint32_t i = 0; i < 32 && i < vk_physical_device_mem_props.memoryTypeCount; i++) {
			if ((typeBits & 1) == 1) {
				// Type is available, does it match requirements ?
				if ((vk_physical_device_mem_props.memoryTypes[i].propertyFlags & requirementsBits) == requirementsBits) {
					return i;
				}
			}
			typeBits >>= 1;
		}
		return -1;
	}

}