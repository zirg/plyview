#include "swapchain.h"

namespace pv {
	SwapChain::SwapChain(pv::PhysicalDevice& physical, pv::WindowKHR& window, Device& device, pv::Renderer& renderer)
		: _image_count(0), _phys(physical), _win(window), _device(device), _renderer(renderer)
	{
		_depth = new pv::DepthBuf(physical, device, window.width, window.height);

		vk_swapchain = VK_NULL_HANDLE;
		createSwapChain();
		_image_count = countImages();
		createImages();
		createViews();
		createCommands();
		createFramebuffers();
	}
	
	SwapChain::~SwapChain() {
	}

	uint32_t			SwapChain::getSwapChainImageCount() {
		return _image_count;
	}


	VkSwapchainKHR					SwapChain::createSwapChain() {
		VkResult					r;
		VkSwapchainKHR				old_swap_chain = vk_swapchain;
		VkSurfaceCapabilitiesKHR	surf_cap;
		VkSwapchainCreateInfoKHR	swap_info;
		uint32_t					presentModeCount = 0;
		VkExtent2D					swapchainExtent;

		std::memset(&swap_info, 0, sizeof(VkSwapchainCreateInfoKHR));
		swap_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;

		// Check the surface capabilities and formats
		r = _win.fpGetPhysicalDeviceSurfaceCapabilitiesKHR(_phys.vk_physical_device, _win.vk_surface, &surf_cap);
		PV_ASSERT_VK(r, VK_SUCCESS, "Get surface capabilities failed");

		r = _win.fpGetPhysicalDeviceSurfacePresentModesKHR(_phys.vk_physical_device, _win.vk_surface, &presentModeCount, NULL);
		PV_ASSERT_VK(r, VK_SUCCESS, "get surface present modes count failed");

		VkPresentModeKHR *presentModes = new VkPresentModeKHR[presentModeCount];
		r = _win.fpGetPhysicalDeviceSurfacePresentModesKHR(_phys.vk_physical_device, _win.vk_surface, &presentModeCount, presentModes);
		PV_ASSERT_VK(r, VK_SUCCESS, "get surface present modes failed");

		if (surf_cap.currentExtent.width == (uint32_t)-1) { // If the surface size is undefined, the size is set to the size of the images requested.
			swapchainExtent.width = _win.width;
			swapchainExtent.height = _win.height;
		}
		else { 	// If the surface size is defined, the swap chain size must match
			swapchainExtent = surf_cap.currentExtent;
			_win.width = surf_cap.currentExtent.width;
			_win.height = surf_cap.currentExtent.height;
		}

		// If mailbox mode is available, use it, as is the lowest-latency non-
		// tearing mode.  If not, try IMMEDIATE which will usually be available,
		// and is fastest (though it tears).  If not, fall back to FIFO which is
		// always available.
		VkPresentModeKHR	swapchainPresentMode = VK_PRESENT_MODE_FIFO_KHR;
		for (size_t i = 0; i < presentModeCount; i++) {
			if (presentModes[i] == VK_PRESENT_MODE_MAILBOX_KHR) {
				swapchainPresentMode = VK_PRESENT_MODE_MAILBOX_KHR;
				break;
			}
			if ((swapchainPresentMode != VK_PRESENT_MODE_MAILBOX_KHR) &&
				(presentModes[i] == VK_PRESENT_MODE_IMMEDIATE_KHR)) {
				swapchainPresentMode = VK_PRESENT_MODE_IMMEDIATE_KHR;
			}
		}

		// Determine the number of VkImage's to use in the swap chain (we desire to
		// own only 1 image at a time, besides the images being displayed and
		// queued for display):
		uint32_t desiredNumberOfSwapchainImages = surf_cap.minImageCount + 1;
		if ((surf_cap.maxImageCount > 0) && (desiredNumberOfSwapchainImages > surf_cap.maxImageCount)) {
			// Application must settle for fewer images than desired:
			desiredNumberOfSwapchainImages = surf_cap.maxImageCount;
		}

		VkSurfaceTransformFlagsKHR preTransform;
		if (surf_cap.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR) {
			preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
		}
		else {
			preTransform = surf_cap.currentTransform;
		}

		swap_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
		swap_info.pNext = NULL;
		swap_info.surface = _win.vk_surface;
		swap_info.minImageCount = desiredNumberOfSwapchainImages;
		swap_info.imageFormat = _win.vk_format; //Default is VK_FORMAT_B8G8R8A8_UNORM; 
		swap_info.imageColorSpace = _win.vk_color_space; //No choice, only one value is available VK_COLORSPACE_SRGB_NONLINEAR_KHR;
		swap_info.imageExtent.width = swapchainExtent.width;
		swap_info.imageExtent.height = swapchainExtent.height;
		swap_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
		swap_info.preTransform = (VkSurfaceTransformFlagBitsKHR)preTransform;
		swap_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
		swap_info.imageArrayLayers = 1;
		swap_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		swap_info.queueFamilyIndexCount = 0;
		swap_info.pQueueFamilyIndices = NULL;
		swap_info.presentMode = swapchainPresentMode;
		swap_info.oldSwapchain = old_swap_chain;
		swap_info.clipped = true;

		r = _device.fpCreateSwapchainKHR(_device.vk_device, &swap_info, NULL, &vk_swapchain);
		PV_ASSERT_VK(r, VK_SUCCESS, "Create swap chain failed");
		return vk_swapchain;
	}

	uint32_t			SwapChain::countImages() {
		VkResult		r;
		uint32_t		count = 0;

		r = _device.fpGetSwapchainImagesKHR(_device.vk_device, vk_swapchain, &count, NULL);
		PV_ASSERT_VK(r, VK_SUCCESS, "Get swap chain image count failed");
		return count;
	}

	void				SwapChain::createImages() {
		VkResult		r;
		vk_images = new VkImage[_image_count];
		
		r = _device.fpGetSwapchainImagesKHR(_device.vk_device, vk_swapchain, &_image_count, vk_images);
		PV_ASSERT_VK(r, VK_SUCCESS, "Get swap chain Images failed");

		for (uint32_t i = 0; i < _image_count; i++) {
			_device.initImageLayout(vk_images[i], VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, 0, VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, VK_IMAGE_ASPECT_COLOR_BIT);
		}
	}

	void						SwapChain::createViews() {
		VkResult				r;
		VkImageViewCreateInfo	img_view_info;

		vk_views = new VkImageView[_image_count];
		for (uint32_t i = 0; i < _image_count; i++) {
			
			std::memset(&img_view_info, 0, sizeof(VkImageViewCreateInfo));
			img_view_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
			img_view_info.image = vk_images[i];
			img_view_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
			img_view_info.format = _win.vk_format;
			img_view_info.components.r = VK_COMPONENT_SWIZZLE_R;
			img_view_info.components.g = VK_COMPONENT_SWIZZLE_G;
			img_view_info.components.b = VK_COMPONENT_SWIZZLE_B;
			img_view_info.components.a = VK_COMPONENT_SWIZZLE_A;
			img_view_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			img_view_info.subresourceRange.baseMipLevel = 0;
			img_view_info.subresourceRange.levelCount = 1;
			img_view_info.subresourceRange.baseArrayLayer = 0;
			img_view_info.subresourceRange.layerCount = 1;

	
			r = vkCreateImageView(_device.vk_device, &img_view_info, NULL, &(vk_views[i]));
			PV_ASSERT_VK(r, VK_SUCCESS, "Create image view failed");
		}
	}

	void							SwapChain::createCommands() {
		VkResult					r;
		VkCommandBufferAllocateInfo	cmd_info;
		uint32_t					pool_fam_idx;

		vk_commands = new VkCommandBuffer[_image_count];
		pool_fam_idx = _device.getQueueFamilyIndex(true, VK_QUEUE_GRAPHICS_BIT);
		std::memset(&cmd_info, 0, sizeof(VkCommandBufferAllocateInfo));
		cmd_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		cmd_info.commandPool = _device.getCommandPool(pool_fam_idx);
		cmd_info.commandBufferCount = _image_count;
		cmd_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		r = vkAllocateCommandBuffers(_device.vk_device, &cmd_info, vk_commands);
		PV_ASSERT_VK(r, VK_SUCCESS, "Allocatiing swapchain command buffer failed");
	}

	void							SwapChain::createFramebuffers() {
		VkImageView					attachments[2];
		VkFramebufferCreateInfo		fb_info;
		VkResult					r;

		attachments[1] = _depth->vk_view;

		std::memset(&fb_info, 0, sizeof(VkFramebufferCreateInfo));
		fb_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		fb_info.pNext = NULL;
		fb_info.renderPass = _renderer.vk_renderpass;
		fb_info.attachmentCount = 2;
		fb_info.pAttachments = attachments;
		fb_info.width = _win.width;
		fb_info.height = _win.height;
		fb_info.layers = 1;

		vk_framebuffers = new VkFramebuffer[_image_count];

		for (uint32_t i = 0; i < _image_count; i++) {
			attachments[0] = vk_views[i];
			r = vkCreateFramebuffer(_device.vk_device, &fb_info, NULL, &(vk_framebuffers[i]));
			PV_ASSERT_VK(r, VK_SUCCESS, "Framebuffer creation fail");
		}
	}
}