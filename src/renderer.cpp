#include "renderer.h"

namespace pv {
	Renderer::Renderer(pv::Device& device, pv::WindowKHR& window) 
	: _device(device), _win(window) {
		std::memset(&vk_viewport, 0, sizeof(vk_viewport));
		vk_viewport.width = (float)_win.width;
		vk_viewport.height = (float) _win.height;
		vk_viewport.minDepth = 0.0f;
		vk_viewport.maxDepth = 1.0f;

		std::memset(&vk_scissor, 0, sizeof(vk_scissor));
		vk_scissor.extent.width = _win.width;
		vk_scissor.extent.height = _win.height;
		vk_scissor.offset.x = 0;
		vk_scissor.offset.y = 0;
	}
	
	Renderer::~Renderer() {	}

	void		Renderer::endVkRenderPass(VkCommandBuffer& cmd) {
		vkCmdEndRenderPass(cmd);
	}

	FlatRenderer::FlatRenderer(pv::Device& device, pv::WindowKHR& window) 
	: Renderer::Renderer(device, window) {		
		vk_subpasses_count = 1;
		vk_subpasses = new VkSubpassDescription[1];

		createVkRenderPass();

		pv_programs = new pv::FlatProgram(device);
		pv_programs[0].createVkPipeline(vk_renderpass, 0);

		_clear[0].color.float32[0] = 0.2f;
		_clear[0].color.float32[1] = 0.2f;
		_clear[0].color.float32[2] = 0.2f;
		_clear[0].color.float32[3] = 0.2f;
		_clear[1].depthStencil.depth = 1.0f;
		_clear[1].depthStencil.stencil = 0;
	}

	FlatRenderer::~FlatRenderer() {	}

	void						FlatRenderer::createVkRenderPass() {
		VkResult				r;
		VkRenderPassCreateInfo	rp_info;

		std::memset(_attachments, 0, sizeof(VkAttachmentDescription) * 2);
		_attachments[0].format = _win.vk_format;
		_attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
		_attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		_attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		_attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		_attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		_attachments[0].initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		_attachments[0].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		_attachments[1].format = VK_FORMAT_D16_UNORM;
		_attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
		_attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		_attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		_attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		_attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		_attachments[1].initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
		_attachments[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

		std::memset(&_color_reference, 0, sizeof(VkAttachmentReference));
		_color_reference.attachment = 0;
		_color_reference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

		std::memset(&_depth_reference, 0, sizeof(VkAttachmentReference));
		_depth_reference.attachment = 1;
		_depth_reference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

		std::memset(vk_subpasses, 0, sizeof(VkSubpassDescription));
		vk_subpasses[0].pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		vk_subpasses[0].flags = 0;
		vk_subpasses[0].inputAttachmentCount = 0;
		vk_subpasses[0].pInputAttachments = NULL;
		vk_subpasses[0].colorAttachmentCount = 1;
		vk_subpasses[0].pColorAttachments = &_color_reference;
		vk_subpasses[0].pResolveAttachments = NULL;
		vk_subpasses[0].pDepthStencilAttachment = &_depth_reference;
		vk_subpasses[0].preserveAttachmentCount = 0;
		vk_subpasses[0].pPreserveAttachments = NULL;

		std::memset(&rp_info, 0, sizeof(VkRenderPassCreateInfo));
		rp_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		rp_info.attachmentCount = 2;
		rp_info.pAttachments = _attachments;
		rp_info.subpassCount = vk_subpasses_count;
		rp_info.pSubpasses = vk_subpasses;

		r = vkCreateRenderPass(_device.vk_device, &rp_info, NULL, &vk_renderpass);
		PV_ASSERT_VK(r, VK_SUCCESS, "Render pass creation failed");
	}
	void		FlatRenderer::beginVkRenderPass(VkCommandBuffer& cmd, VkFramebuffer& frame) {
		VkRenderPassBeginInfo	rp_begin_info;

		std::memset(&rp_begin_info, 0, sizeof(VkRenderPassBeginInfo));
		rp_begin_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		rp_begin_info.renderPass = vk_renderpass;
		rp_begin_info.framebuffer = frame;
		rp_begin_info.renderArea.offset.x = 0;
		rp_begin_info.renderArea.offset.y = 0;
		rp_begin_info.renderArea.extent.width = _win.width;
		rp_begin_info.renderArea.extent.height = _win.height;
		rp_begin_info.clearValueCount = 2;
		rp_begin_info.pClearValues = _clear;
		vkCmdBeginRenderPass(cmd, &rp_begin_info, VkSubpassContents::VK_SUBPASS_CONTENTS_INLINE);
	}

	void		FlatRenderer::renderToCmd(VkCommandBuffer& cmd, VkFramebuffer& frame, pv::Mesh& mesh, VkDescriptorSet& set) {
		beginVkRenderPass(cmd, frame);

		pv_programs[0].bind(cmd);

		vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pv_programs[0].vk_pipeline_layout, 0, 1, &set, 0, VK_NULL_HANDLE);

		vkCmdSetViewport(cmd, 0, 1, &vk_viewport);
		vkCmdSetScissor(cmd, 0, 1, &vk_scissor);

		vkCmdBindVertexBuffers(cmd, 0, mesh.vk_buffer_count(), mesh.vk_buffers(), mesh.vk_offsets());
		vkCmdBindIndexBuffer(cmd, mesh.vk_indices(), 0, VK_INDEX_TYPE_UINT32);
		vkCmdDrawIndexed(cmd, mesh.vk_indices_count(), 1, 0, 0, 0);

		endVkRenderPass(cmd);
	}
}