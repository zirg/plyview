#include "plyView.h"

uniformbuf_t						g_uniform;
mat4x4			proj, view, model, cu_model;
mat4x4			VP, MVP;
vec3			v_eye, v_center, v_up;

#ifdef _WIN32

pv::Viewer	*g_app = 0;

// MS-Windows event handling function:
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	if (g_app != 0) {
		switch (uMsg) {
		case WM_CLOSE:
			g_app->pvClose(0);
			PostQuitMessage(0);
			g_app = 0;
			break;
		case WM_PAINT:
			mat4x4_rotate_Y(cu_model, model, 0.1f * 3.14f / 180.0f);
			std::memcpy(model, cu_model, sizeof(model));
			std::memcpy(g_uniform.m, model, sizeof(model));

			// Update mvp matrix
			mat4x4_mul(VP, proj, view);
			mat4x4_mul(MVP, VP, model);
			std::memcpy(g_uniform.mvp, MVP, sizeof(MVP));

			g_app->pvRotate(g_uniform);
			g_app->pvDraw();
			break;
		case WM_SIZE:
			if (wParam != SIZE_MINIMIZED) {
				uint32_t w = lParam & 0xffff;
				uint32_t h = lParam & 0xffff0000 >> 16;
				g_app->pvResize(w, h);
			}
			break;
		default:
			break;
		}
	}
	return (DefWindowProc(hWnd, uMsg, wParam, lParam));
}
#endif

#ifdef _WIN32

//No microsoft, I want the standard C/C++ main
#ifdef _MSC_VER
#    pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif

int				main(int ac, char **av) {
	char		opt;
	char		*file_path = NULL;
	float		scale = 1.0f;

	pv::flat_vertex_shader.assign("shaders/flat.vert.spv");
	pv::flat_fragment_shader.assign("shaders/flat.frag.spv");
	while ((opt = getopt(ac, av, "p:f:v:s:")) != -1) {
		switch (opt) {
		case 'p':
			file_path = optarg;
			break;
		case 'f':
			pv::flat_fragment_shader.assign(optarg);
			break;
		case 'v':
			pv::flat_vertex_shader.assign(optarg);
			break;
		case 's':
			scale = (float)atof(optarg);
			break;
		default:
			break;
		}
	}

	if (file_path == NULL) {
		std::cerr << PV_USAGE << std::endl;
		return 1;
	}


	g_connection = GetModuleHandle(NULL);

	{
		pv::Viewer	app;


		if (app.init(file_path) == false)
			return 1;

		/* Init MVP */
		std::memset(v_eye, 0, sizeof(v_eye));
		std::memset(v_eye, 0, sizeof(v_center));
		std::memset(v_eye, 0, sizeof(v_up));

		v_eye[0] = 0.0f;
		v_eye[1] = 20.0f;
		v_eye[2] = 20.0f;

		v_up[1] = -1.0;

		mat4x4_perspective(proj, 45.0f * 3.14f / 180.0f, 1280.0f / 720.0f, 0.1f, 500.0f);
		mat4x4_identity(view);
		mat4x4_look_at(view, v_eye, v_center, v_up);
		mat4x4_identity(cu_model);
		mat4x4_translate(cu_model, 0.0, -8.0, 0.0);
		mat4x4_scale_aniso(model, cu_model, scale, scale, scale);

		g_app = &app;
		app.run();
	}
	return 0;
}

#else
int							plyViewRun() {
	return 0;
}
int							main(int ac, char **av) {
	const xcb_setup_t *setup;
	xcb_screen_iterator_t iter;
	int scr;

	char		opt;
	char		*file_path = NULL;

	pv::flat_vertex_shader.assign("shaders/flat.vert.spv");
	pv::flat_fragment_shader.assign("shaders/flat.frag.spv");
	while ((opt = getopt(ac, av, "p:f:v:")) != -1) {
		switch (opt) {
		case 'p':
			file_path = optarg;
			break;
		case 'f':
			pv::flat_fragment_shader.assign(optarg);
			break;
		case 'v':
			pv::flat_vertex_shader.assign(optarg);
			break;
		default:
			break;
		}
	}

	if (file_path == NULL) {
		std::cerr << PV_USAGE << std::endl;
		return 1;
	}

	g_connection = xcb_connect(NULL, &scr);
	if (g_connection == NULL) {
		std::cerr << "Cannot find a compatible Vulkan installable client driver " << std::endl;
		return 1;
	}

	setup = xcb_get_setup(g_connection);
	iter = xcb_setup_roots_iterator(setup);
	while (scr-- > 0)
		xcb_screen_next(&iter);

	g_screen = iter.data;
	plyViewInit(file_path);
	return plyViewRun();
}
#endif