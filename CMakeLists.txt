cmake_minimum_required(VERSION 3.0 FATAL_ERROR)

### PROJECT
#Project declaration
project(plyView)

set(plyViewSrc		${PROJECT_SOURCE_DIR}/src/main.cpp
					${PROJECT_SOURCE_DIR}/src/viewer.cpp
					${PROJECT_SOURCE_DIR}/src/context.cpp
					${PROJECT_SOURCE_DIR}/src/physical_device.cpp
					${PROJECT_SOURCE_DIR}/src/window.cpp
					${PROJECT_SOURCE_DIR}/src/device.cpp
					${PROJECT_SOURCE_DIR}/src/swapchain.cpp
					${PROJECT_SOURCE_DIR}/src/depth.cpp
					${PROJECT_SOURCE_DIR}/src/mesh.cpp
					${PROJECT_SOURCE_DIR}/src/program.cpp
					${PROJECT_SOURCE_DIR}/src/renderer.cpp					
					${PROJECT_SOURCE_DIR}/src/plyfile.c
					${PROJECT_SOURCE_DIR}/src/wingetopt.c
					)
set(plyViewInc 		${PROJECT_SOURCE_DIR}/includes
					)


list(APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake")
					

if(WIN32)
  set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -D_CRT_SECURE_NO_WARNINGS -DVK_USE_PLATFORM_WIN32_KHR -DWIN32_LEAN_AND_MEAN")
  set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -D_CRT_SECURE_NO_WARNINGS -DVK_USE_PLATFORM_WIN32_KHR -DWIN32_LEAN_AND_MEAN")
elseif(UNIX)
  find_package(XCB REQUIRED)
  set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DVK_USE_PLATFORM_XCB_KHR")
  set (CMAKE_CXX_FLAGS "${CMAKE_C_FLAGS} -DVK_USE_PLATFORM_XCB_KHR")
  include_directories (${XCB_INCLUDE_DIRS})
  link_libraries(${XCB_LIBRARIES})
else()
endif()

find_package (Vulkan REQUIRED)

if (WIN32)
add_executable(${PROJECT_NAME} WIN32 ${plyViewSrc})

else()
add_executable(${PROJECT_NAME} ${plyViewSrc})
endif()
target_include_directories(${PROJECT_NAME} PUBLIC ${plyViewInc} ${VULKAN_INCLUDE_DIR})
target_link_libraries(${PROJECT_NAME} ${VULKAN_LIBRARIES})